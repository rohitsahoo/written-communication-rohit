## 1. What is __Scaling__?
Scalability is a characteristic of an system or project that describes its capability to grow in time and perform well under an increased or expanding workload. 

## 2. What are the problems of Scaling?
- Limited physical resources like memory, CPUs, etc.,
- Wrong memory management
- Inefficient database engine
- Poorly performed database queries
- Wrong server configuration
- And many more...

## 3. How To Do Efficient Scaling?
- By keeping your code clean
- Choose right databse engine
- Choose right server for hosting
- Make your database queries efficient
- Keep development and production as similar as possible

## 4. What is __Caching__?
Caching is the process of storing most frequently used data in a temporary buffer to improve the performance. In this scenario, the most frequently accessed data is not retrieved from the original source every time.

## 5. Common Scalability scenarios for _Caching_
- Vertical Scalability  
  > This can be achieved by upgrading the single machine. Like increasing the memory, ram or upgrading the processor of the single machine.

- Horizontal Scalability  
  > This can be achieved by increasing the number of machines. Like having four or five servers (as per the requirement) instead of upgrading one server.

- In-Process Caching  
  > In-Process cache is an object cache built within the same memory address space as your application. In this case the elements are local to a single instance of the application.

- In-Memory Distributed Caching
  > A distributed cache is a system that pools together the Random Access Memory (RAM) of multiple networked computers into a single in-memory data store, used as a data cache to provide fast access to data. 

- In-Memory Database
  > In this technique the database is stored in the RAM instead of a hard disk for faster performance. This database is also called main memory database.


### References:
- [https://dzone.com/articles/application-scalability-how-to-do-efficient-scalin](https://dzone.com/articles/application-scalability-how-to-do-efficient-scalin)
- [https://12factor.net/](https://12factor.net/)
- [https://www.investopedia.com/terms/s/scalability.asp](https://www.investopedia.com/terms/s/scalability.asp)
- [https://dzone.com/articles/introducing-amp-assimilating-caching-quick-read-fo](https://dzone.com/articles/introducing-amp-assimilating-caching-quick-read-fo)
- [https://hazelcast.com/glossary/distributed-cache/#:~:text=A%20distributed%20cache%20is%20a,provide%20fast%20access%20to%20data.&text=Distributed%20caches%20are%20especially%20useful,high%20data%20volume%20and%20load](https://hazelcast.com/glossary/distributed-cache/#:~:text=A%20distributed%20cache%20is%20a,provide%20fast%20access%20to%20data.&text=Distributed%20caches%20are%20especially%20useful,high%20data%20volume%20and%20load)
- [https://stackoverflow.com/questions/33332288/in-process-cache-vs-distributed-cache-on-consistency-with-mutable-immutable-obje](https://stackoverflow.com/questions/33332288/in-process-cache-vs-distributed-cache-on-consistency-with-mutable-immutable-obje)


### Author  
Rohit Sahoo - _rohit.sahoo@mountblue.tech_
